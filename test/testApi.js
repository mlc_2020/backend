var mocha = require("mocha"); // JavaScript Test Framework
var chai = require("chai"); // Aserciones
var chaihttp = require("chai-http"); // Peticiones HTTP
//Aumentamos la funcionalidad de chai, con este plugin para poder lanzar peticiones
//http
chai.use(chaihttp);
var should = chai.should();

describe('Test de API TechU',//Suite de test unitario
    function () {//Funcion manejadora de una suite
        it('apitechu/v2/users - Lista de usuarios', //Test unitario
            function (done) {
                chai.request('http://localhost:3001')
                    .get('/apitechu/v2/users')
                    .end(
                        function (err, res) {
                            res.should.have.status(200);
                            res.body.should.be.a('object');
                            for (users of res.body.users) {
                                users.should.have.property('id_user');
                                users.should.have.property('email');
                            }
                            done();
                        })
            })

        it('apitechu/v2/users/:userId/accounts - Lista de cuentas por usuario', //Test unitario
            function (done) {
                chai.request('http://localhost:3001')
                    .get('/apitechu/v2/users/1/accounts')
                    .end(
                        function (err, res) {
                            res.should.have.status(200);
                            res.body.should.be.a('object');
                            for (users of res.body.accounts) {
                                users.should.have.property('cardTitle');
                                users.should.have.property('numProduct');
                            }
                            done();
                        })
            })

        it('apitechu/v2/users - Inserta un nuevo usuario', //Test unitario
            function (done) {
                chai.request('http://localhost:3001')
                    .post('/apitechu/v2/users')
                    .send({
                        first_name: "Test",
                        last_name: "Test",
                        email: "test@test.com",
                        password: "",
                        accounts: []
                    })
                    .end(
                        function (err, res) {
                            res.should.have.status(200);
                            res.body.should.be.a('object'); 
                            res.body.should.have.property('id_user'); 
                            done();
                        })
            })
    }


);
