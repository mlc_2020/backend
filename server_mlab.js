require('dotenv').config();
const express = require('express');
const bodyParser = require('body-parser');
const requestJson = require('request-json');
const swaggerUi = require('swagger-ui-express'),
swaggerDocument = require('./swagger.json');

const cors = require('cors');
const app = express();
const port = process.env.PORT || 3000;
const URL_BASE = '/apitechu/v2/';
const URL_MLAB = 'https://api.mlab.com/api/1/databases/techu23db/collections/';
const API_KEY = 'apiKey=' + process.env.API_KEY_MLAB;
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies
app.use(cors());
app.options('*', cors());

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

app.listen(port, function () {
  console.log('Node JS escuchando en el puerto' + port);
});

app.get(URL_BASE + 'users',
  function (req, res) {
    const httpClient = requestJson.createClient(URL_MLAB);
    console.log("Cliente HTTP mLab creado.");
    httpClient.get('users?' + API_KEY,
      function (err, respuestaMLab, body) {
        console.log('Error: ' + JSON.stringify(err));
        console.log('Respuesta MLab: ' + JSON.stringify(respuestaMLab));
        console.log('Body: ' + JSON.stringify(body));
        var response = {};
        if (err) {
          response = { "msg": "Error al recuperar users de mLab." }
          res.status(500);
        } else {
          if (body.length > 0) {
            response = {users: body};
          } else {
            response = { "msg": "Usuario no encontrado." };
            res.status(404);
          }
        }
        res.send(response);
      }); //httpClient.get(
  });

app.get(URL_BASE + 'users/:id',
  (request, response) => {
    console.log(request.params.id);
    let id = request.params.id;
    let queryString = `q={"id_user":${id}}&`;
    let fieldString = 'f={"_id":0}&';
    const httpClient = requestJson.createClient(URL_MLAB);
    httpClient.get('users?' + queryString + fieldString + API_KEY, (error, resMlab, body) => {
      let res = {};
      if (error) {
        res = { 'msg': 'Error en la peticion a mLab' };
        response.status(500);
      } else {
        if (body.length > 0) {
          res = body;
        } else {
          response.status(404);
          res = { 'msg': 'Usuario no encontrado' };
        }
      }
      response.send(res);
    });
  });

app.get(URL_BASE + 'usersMail/:email',
  (request, response) => {
    console.log(request.params.id);
    let email = request.params.email;
    let queryString = `q={"email":"${email}"}&`;
    let fieldString = 'f={"_id":0}&';
    const httpClient = requestJson.createClient(URL_MLAB);
    httpClient.get('users?' + queryString + fieldString + API_KEY, (error, resMlab, body) => {
      let res = {};
      if (error) {
        res = { 'msg': 'Error en la peticion a mLab' };
        response.status(500);
      } else {
        if (body.length > 0) {
          res = {user: body};
        } else {
          response.status(404);
          res = { 'msg': 'Usuario no encontrado' };
        }
      }
      response.send(res);
    });
  });

app.get(URL_BASE + 'users/:id/accounts',
  (request, response) => {
    console.log(request.params.id);
    let id = request.params.id;
    let queryString = `q={"id_user":${id}}&`;
    let fieldString = 'f={"_id":0,"accounts":1}&';

    const httpClient = requestJson.createClient(URL_MLAB);
    httpClient.get('users?' + queryString + fieldString + API_KEY, (error, resMlab, body) => {
      let res = {};
      if (error) {
        res = { 'msg': 'Error en la peticion a mLab' };
        response.status(500);
      } else {
        if (body.length > 0) {
          res = body[0];
        } else {
          response.status(404);
          res = { 'msg': 'Cuentas no encontradas' };
        }
      }
      response.send(res);
    });
  });

//POST of user
app.post(URL_BASE + "users",
  function (req, res) {
    var clienteMlab = requestJson.createClient(URL_MLAB);
    let queryString = `s={"id_user":-1}&l=1&`;
    let fieldString = `f={"_id":0}&`;
    clienteMlab.get('users?' + queryString + fieldString + API_KEY,
      function (error, respuestaMLab, data) { 
        console.log('data::', data);
        if(data.length > 0) {
          newID = data[0].id_user + 1;
        } else {
          newID = 1;
        }
        
        console.log("newID:" + newID);
        var newUser = {
          "id_user": newID,
          "first_name": req.body.first_name,
          "last_name": req.body.last_name,
          "email": req.body.email,
          "password": req.body.password,
          "accounts": req.body.accounts
        };
        clienteMlab.post(URL_MLAB + "users?" + API_KEY, newUser,
          function (error, respuestaMLab, body) {
            res.send(body);
          });
      });
  });

app.put(URL_BASE + 'users/:id',
  (req, res) => {
    let id = req.params.id;
    let queryString = 'q={"id_user":' + id + '}&';
    const http_client = requestJson.createClient(URL_MLAB);
    http_client.get('users?' + queryString + API_KEY,
      (err, resMLab, body) => {
        var cambio = '{"$set":' + JSON.stringify(req.body) + '}';
        console.log(cambio);
        http_client.put(URL_MLAB + 'users?' + queryString + API_KEY, JSON.parse(cambio),
          (error, respuestaMLab, body) => {
            console.log("body:" + body); // body.n == 1 si se pudo hacer el update
            //res.status(200).send(body);
            (body.n == 1) ? res.status(200).send(body) :  res.status(404).send(body);
          });
      });
  });

app.patch(URL_BASE + 'users/:id',
  (req, res) => {
    let id = req.params.id;
    let queryString = 'q={"id_user":' + id + '}&';
    const http_client = requestJson.createClient(URL_MLAB);
    http_client.get('users?' + queryString + API_KEY,
      (err, resMLab, body) => {
        var cambio = '{"$set": { "accounts" : ' + JSON.stringify(req.body.accounts) + ' } }';
        console.log(cambio);
        http_client.put(URL_MLAB + 'users?' + queryString + API_KEY, JSON.parse(cambio),
          (error, respuestaMLab, body) => {
            console.log("body:" + body); // body.n == 1 si se pudo hacer el update
            //res.status(200).send(body);
            (body.n == 1) ? res.status(200).send(body) :  res.status(404).send(body);
          });
      });
  });

//Method POST login
app.post(URL_BASE + "login",
  function (req, res) {
    console.log("POST /colapi/v3/login");
    let email = req.body.email;
    let pass = req.body.password;
    let queryString = 'q={"email":"' + email + '","password":"' + pass + '"}&';
    let limFilter = 'l=1&';
    let clienteMlab = requestJson.createClient(URL_MLAB);
    clienteMlab.get('users?' + queryString + limFilter + API_KEY,
      function (error, respuestaMLab, body) {
        if (!error) {
          if (body.length == 1) { // Existe un usuario que cumple 'queryString'
            let login = '{"$set":{"logged":true}}';
            clienteMlab.put('users?q={"id_user": ' + body[0].id_user + '}&' + API_KEY, JSON.parse(login),
              //clienteMlab.put('user/' + body[0]._id.$oid + '?' + apikeyMLab, JSON.parse(login),
              function (errPut, resPut, bodyPut) {
                res.send({ 'msg': 'Login correcto', 'user': body[0].email, 'userid': body[0].id_user, 'name': body[0].first_name });
                // If bodyPut.n == 1, put de mLab correcto
              });
          }
          else {
            res.status(404).send({ "msg": "Usuario no válido." });
          }
        } else {
          res.status(500).send({ "msg": "Error en petición a mLab." });
        }
      });
  });
//Method POST logout
app.post(URL_BASE + "logout",
  function (req, res) {
    console.log("POST /colapi/v3/logout");
    var email = req.body.email;
    var queryString = 'q={"email":"' + email + '","logged":true}&';
    console.log(queryString);
    var clienteMlab = requestJson.createClient(URL_MLAB);
    clienteMlab.get('users?' + queryString + API_KEY,
      function (error, respuestaMLab, body) {
        var respuesta = body[0]; // Asegurar único usuario
        if (!error) {
          if (respuesta != undefined) { // Existe un usuario que cumple 'queryString'
            let logout = '{"$unset":{"logged":true}}';
            clienteMlab.put('users?q={"id_user": ' + respuesta.id_user + '}&' + API_KEY, JSON.parse(logout),
              function (errPut, resPut, bodyPut) {
                res.send({ 'msg': 'Logout correcto', 'user': respuesta.email });
                // If bodyPut.n == 1, put de mLab correcto
              });
          } else {
            res.status(404).send({ "msg": "Logout failed!" });
          }
        } else {
          res.status(500).send({ "msg": "Error en petición a mLab." });
        }
      });
  });


//get movimientos por usuario y cuenta
app.get(URL_BASE + 'movements/:idUs/:idAcc',
(request, response) => {
  console.log(request.params.idUs);
  console.log(request.params.idAcc);
  let idUs = request.params.idUs;
  let idAcc = request.params.idAcc;
  let queryString = `q={"id_user":${idUs},"id_account":"${idAcc}"}&`;
  const httpClient = requestJson.createClient(URL_MLAB);
  httpClient.get('movements?' + queryString + API_KEY, (error, resMlab, body) => {
    console.log('Body: ' + JSON.stringify(body));
    let res = {};
    if (error) {
      res = { 'msg': 'Error en la peticion a mLab' };
      response.status(500);
    } else {
      if (body.length > 0) {
        res = {movements: body};
      } else {
        response.status(404);
        res = { 'msg': 'Movimientos no encontradas' };
      }
    }
    response.send(res);
  });
});


//POST  movimientos
app.post(URL_BASE + "movements",
function (req, res) {
  var clienteMlab = requestJson.createClient(URL_MLAB);
  let queryString = `s={"id_movimiento":-1}&l=1&`;
  let fieldString = `f={"_id":0}&`;
  clienteMlab.get('movements?' + queryString + fieldString + API_KEY,
    function (error, respuestaMLab, data) {
      console.log('data::', data);
      if(data.length > 0) {
        newID = data[0].id_movimiento + 1;
      } else {
        newID = 1;
      }
      console.log("newID:" + newID);
      console.log("newBODY:" + JSON.stringify(req.body));
      var newMov = {
        "id_movimiento": newID,
        "id_user": req.body.id_user,
        "id_account": req.body.id_account,
        "descripcion": req.body.descripcion,
        "moneda": req.body.moneda,
        "monto": req.body.monto,
        "comercio": req.body.comercio
      };
      clienteMlab.post(URL_MLAB + "movements?" + API_KEY, newMov,
        function (error, respuestaMLab, body) {
          res.send(body);
        });
    });
}); 