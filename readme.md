# Proyecto BBVA 

Este aplicativo esta diseñado para el proyecto final de Tech University - Perú(1° edición 2020) del BBVA.

Todo el código esta alojado en bitbucket en el siguiente repositorio: https://bitbucket.org/mlc_2020/backend/src/master/

En este repositorio se encuentra todo lo relacionado al backend del proyecto, donde se trabajo la capa de servicios.

## Introducción

El aplicativo esta desarrollado para un entorno de banca móvil(webapp),con las siguientes funcionalidades:

1.-Consultar Movimientos
2.-Dar de alta movimientos
3.-Dar de alta Usuarios
4.-Mostrar datos de otras api
5.-Gestionar Logins
6.-Dar de alta cuentas
7.-Listar cuentas de Usuario

## Casos de Uso 
### GET:
- **Usuarios**
		
		-**/apitechu/v2/users**-> Devuelve todos los usuarios registrados.
		-**/apitechu/v2/users/:id**->Busca un usuario por id, retorna la informacion del usuario
		-**/apitechu/v2/users/:id/accounts**-> Devuelve las cuentas registradas de cada usuario y su informacion correspondiente
		-**/apitechu/v2/usersMail/:email**-> Devuelve un usuario buscando por email.

- **Movimientos**		
		
		-**/apitechu/v2/movements/:idUs/:idAcc**-> Devuelve todos los movimientos de una cuenta, recibiendo como parametro el id_user y el id_account.
		-**/apitechu/v2/movements**-> Inserta movimientos de una cuenta, enviando datos como el id del usuario,id de la cuenta, descripcion del movimiento, moneda del movimiento, monto y nombre del comercio donde se hizo la operación.
		
### POST 
- **Usuarios**
		
		-**/apitechu/v2/users**-> A partir de un JSON que se le pasa con la data del usuario, crea un nuevo usuario en la colleccion users.
		
- **Login**

		-**/apitechu/v2/login**-> Si el usuario con el email y contraseña enviado existe le setea la variable logged a true.
		-**/apitechu/v2/logout**-> Valida que el usuario exista y tenga la variabla logged en true, de ser asì, elimina la variable logged del usuario.
		
- **Movimientos**		
		
		-**/apitechu/v2/movements**-> A partir de un JSON que se le pasa con la data de un movimiento, crea un nuevo movimiento en la colleccion movements. 
		
		
		
### PUT 
- **Usuarios**
		
		-**/apitechu/v2/users/:id**-> Editamos al usuario a traves de un JSON con sus campos, los datos para modificar se pasaran por JSON. 
		
### PATCH 
- **Usuarios**
		
		-**/apitechu/v2/users/:id**-> A partir de un JSON donde se le envia las cuentas a agregar, setea nuevas cuentas al usuario encontrado por id. 

### Requisitos "Adicionales" implementados		

A parte de los requisitos minimos exigidos para superar la práctica se han implementado los siguientes requisitos adicionales que se ofrecían:  

- Implementacion de Swagger para documentacion de APIS, ingresando a http://localhost:3001/api-docs/

- Implementacion de pruebas unitarias usando las librerias de mocha y chai,digitando estos comandos: 

`npm run dev`

`npm run test`
