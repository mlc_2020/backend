#Imagen 
FROM node:latest

#Crear directorio de trabajo del contenedor docker
WORKDIR /docker-dir-apitechu

# Copiar archivos del proyecto en el directorio de trabajo Docker
ADD . /docker-dir-apitechu

#Instalar dependencias produccion del proyecto
# RUN npm instal --only=production

#Puerto donde exponemos contenedor
EXPOSE 3001

#Comando para lanzar la app
CMD ["npm", "run", "prod"]
