require('dotenv').config();
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const app = express();
var port = process.env.PORT || 3000;
const URL_BASE = '/apitechu/v1/';
const usersFile = require('./users.json');

app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

app.use(cors());
app.options('*', cors());

app.listen(port, function () {
  console.log('Node JS escuchando en el puerto' + port);
});

app.get(URL_BASE + 'users',
  function (request, response) {
    console.log('Hola mundo');
    response.send(usersFile);
  }
);

app.get(URL_BASE + 'users/:id',
  function (request, response) {
    console.log(request.params.id);
    const user = usersFile.filter(e => e.id_user == request.params.id);
    let respuesta = (user[0] == undefined) ? { 'msg': 'usuario no existe' } : user[0]
    let status_code = (user[0] == undefined) ? 404 : 200;
    response.status(status_code).send(respuesta);
  });

app.get(URL_BASE + 'total_users',
  function (request, response) {
    const totalUsers = usersFile.length;
    let status_code = (totalUsers == 0) ? 200 : 200;
    console.log(totalUsers)
    response.status(status_code).send({"num_usuarios":totalUsers});
  });



app.get(URL_BASE + 'usersMoreParams/:id/:firstname/:email',
  function (request, response) {
    const id = request.params.id;
    const firstName = request.params.firstname;
    const email = request.params.email;
    const user = usersFile.filter(e => e.id_user == id && e.first_name == firstName && e.email == email);
    let respuesta = (user[0] == undefined) ? { 'msg': 'usuario no existe' } : user[0]
    let status_code = (user[0] == undefined) ? 404 : 200;
    response.status(status_code).send(respuesta);
  });


app.post(URL_BASE + 'users',
  function (req, res) {
    let usersFileOrder = usersFile.sort((a, b) => (a.id_user < b.id_user) ? 1 : ((b.id_user < a.id_user) ? -1 : 0));
    let newUser = {
      'id_user': usersFileOrder[0].id_user + 1,
      'first_name': req.body.first_name,
      'last_name': req.body.last_name,
      'email': req.body.email,
      'password': req.body.password
    };
    console.log(newUser);
    usersFile.push(newUser);
    res.status(201).send('Usuario registrado');
  }
);

app.put(URL_BASE + 'users',
  function (req, res) {
    const idUser = req.body.id_user;
    const userIndex = usersFile.findIndex(e => e.id_user == idUser);

    let newUser = {
      'id_user': idUser,
      'first_name': req.body.first_name,
      'last_name': req.body.last_name,
      'email': req.body.email,
      'password': req.body.password
    };
    usersFile[userIndex] = newUser;
    res.send(usersFile);
  }
);

app.delete(URL_BASE + 'users/:id',
  (req, res) => {
    const idUser = req.params.id;
    const userIndex = usersFile.findIndex(e => e.id_user == idUser);
    usersFile.splice(userIndex, 1);
    res.send(usersFile);
  }
);

app.get(URL_BASE + 'usersq',
  (req, res) => {
    console.log(req.query.id);
    console.log(req.query.country);
    res.send("Get con query");
  }
);

app.post(URL_BASE + 'login',
  function (request, response) {
    console.log("POST /apicol/v2/login");
    console.log(request.body.email);
    console.log(request.body.password);
    var user = request.body.email;
    var pass = request.body.password;
    for (us of usersFile) {
      if (us.email == user) {
        if (us.password == pass) {
          us.logged = true;
          writeUserDataToFile(usersFile);
          console.log("Login correcto!");
          response.send({ "msg": "Login correcto.", "idUsuario": us.id_user });
        } else {
          console.log("Login incorrecto.");
          response.send({ "msg": "Login incorrecto." });
        }
      }
    }
  });

// LOGOUT - users.json
app.post(URL_BASE + 'logout',
  function (request, response) {
    console.log("POST /apicol/v2/logout");
    var userId = request.body.id;
    let usersFile = require('./users.json');
    for (us of usersFile) {
      if (us.id_user == userId) {
        if (us.logged) {
          delete us.logged; // borramos propiedad 'logged'
          writeUserDataToFile(usersFile);
          console.log("Logout correcto!");
          response.send({ "msg": "Logout correcto.", "idUsuario": us.id_user });
        } else {
          console.log("Logout incorrecto.");
          response.send({ "msg": "Logout incorrecto." });
        }
      } us.logged = true
    }
  });

function writeUserDataToFile(data) {
  var fs = require('fs');
  var jsonUserData = JSON.stringify(data);
  fs.writeFile("./users.json", jsonUserData, "utf8",
    function (err) { //función manejadora para gestionar errores de escritura
      if (err) {
        console.log(err);
      } else {
        console.log("Datos escritos en 'users.json'.");
      }
    }
  )
};